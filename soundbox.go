package main

import (
	"fyne.io/fyne"
	"fyne.io/fyne/app"
	"fyne.io/fyne/widget"
)

func main() {
	app := app.New()

	soundbox := newSoundbox()
	soundbox.loadUI(app)
}

type soundbox struct {
	currentDir *widget.Label
	buttons    map[string]*widget.Button
	window     fyne.Window
}

func newSoundbox() *soundbox {
	buttons := make(map[string]*widget.Button)

	return &soundbox{
		buttons: buttons,
	}
}

func (s *soundbox) loadUI(app fyne.App) {
	s.currentDir = widget.NewLabel("test")

	s.window = app.NewWindow("Soundbox")
}
